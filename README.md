#Trabajo Final

##Finalidad

La finalidad de este trabajo es explorar y trabajar las distintas opciones de personalización que nos da el lenguaje de flexbox. 
Todos los casos están ubicados en el índice index.html.

###Descripción de los casos

**Caso 1** Se utiliza el comando flex-direction para ubicar verticalmente a los div hijos.

**Caso 2** Se utiliza el comando justify-content para desplazar los div hijos hacia la izquierda de la página.

**Caso 3** Se utiliza el comando justify-content para establecer espacios entre los div hijos.

**Caso 4** Se utiliza el comando justify-content para establecer espacios entre los div hijos, pero más espacio incluso que el caso anterior.

**Caso 5** Se utiliza el comando justify-content para ubicar a los div hijos a la altura media del div padre.

**Caso 6** Se utiliza el comando align-content para ubicar a los div hijos en la esquina superior derecha, guardando márgenes.

**Caso 7** Se utiliza el comando align-content para alargar a los div hijos hasta el final del div padre.

**Caso 8** Se utiliza el comando align-content para ubicar a los div hijos en la esquina inferior derecha, guardando márgenes.

**Caso 9** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para 
establecer espacios entre las líneas de  div hijos.

**Caso 10** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para establecer espacios entre las líneas de  div hijos, incluso más que en el caso anterior.

**Caso 11** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para alargar los div hijos, los de la fila de arriba hasta el principio de la línea de abajo, y los de la línea de abajo hasta el final del div padre.

**Caso 12** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para ubicar a la altura media del div padre.

**Caso 13** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para ubicar a los div hijos en la esquina superior derecha.

**Caso 14** Se utiliza el comando flex-wrap para respetar el ancho de los div hijos a la hora de ubicarlos en una línea y el comando align-content para ubicar a los div hijos en la esquina inferior derecha.

**Caso 15** Se utiliza el comando flex-grow para establecer la proporción de espacio que ocupa cada div hijo en el div padre

**Caso 16** Se utiliza el comando flex-wrap para establecer que todos los div hijos estén en una línea, sin respetar el ancho de éstos.
